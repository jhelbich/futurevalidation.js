/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.future.render.kit;

import java.util.Iterator;
import javax.faces.context.FacesContext;
import javax.faces.render.RenderKit;
import javax.faces.render.RenderKitFactory;

/**
 *
 * @author Jan Helbich
 */
public class ValidationRenderKitFactory extends RenderKitFactory {

    private final RenderKitFactory wrapped;

    public ValidationRenderKitFactory(RenderKitFactory wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void addRenderKit(String string, RenderKit rk) {
        wrapped.addRenderKit(string, rk);
    }

    @Override
    public RenderKit getRenderKit(FacesContext fc, String renderKitId) {
        RenderKit kit = wrapped.getRenderKit(fc, renderKitId);
        if (HTML_BASIC_RENDER_KIT.equals(renderKitId)) {
            return new ValidationRenderKit(kit);
        }
        return kit;
    }

    @Override
    public Iterator<String> getRenderKitIds() {
        return wrapped.getRenderKitIds();
    }

    @Override
    public RenderKitFactory getWrapped() {
        return null;
    }

}
