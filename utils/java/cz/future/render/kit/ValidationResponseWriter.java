/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.future.render.kit;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.context.ResponseWriterWrapper;

/**
 * JSF response writer for the 'validation' attribute support in JSF versions
 * lower than 2.2.
 * @author Jan Helbich
 */
public class ValidationResponseWriter extends ResponseWriterWrapper {

    private static final String VALIDATOR_ATTRIBUTE = "validation";
    private static final String ELEMENT_NAME = "input";
    private final ResponseWriter wrapped;

    public ValidationResponseWriter(ResponseWriter wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void startElement(String name, UIComponent component) throws IOException {
        // bugs in PrimeFaces versions 3.4.2 and lower
        if (component == null) {
            component = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
        }
        super.startElement(name, component);

        if (component != null && ELEMENT_NAME.equals(name)) {
            Map<String, Object> attributes = component.getAttributes();
            if (attributes != null) {
                Object attributeValue = attributes.get(VALIDATOR_ATTRIBUTE);

                if (attributeValue != null) {
                    super.writeAttribute(VALIDATOR_ATTRIBUTE, attributeValue, null);
                }
            }
        }
    }

    @Override
    public ResponseWriter cloneWithWriter(Writer writer) {
        return new ValidationResponseWriter(super.cloneWithWriter(writer));
    }

    @Override
    public ResponseWriter getWrapped() {
        return wrapped;
    }

}
