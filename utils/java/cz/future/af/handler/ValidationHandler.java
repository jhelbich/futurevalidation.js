
package cz.future.af.handler;;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Jan Helbich
 */
public class ValidationHandler {

    // well, it is just a just a quick prototype
    private static final Set<String> allowedVars = Sets.newHashSet("email",
                                                                   "minLength",
                                                                   "maxLength",
                                                                   "max",
                                                                   "min",
                                                                   "required",
                                                                   "future",
                                                                   "past",
                                                                   "javaPattern",
                                                                   "pattern",
                                                                   "compositionType",
                                                                   "creditCardNumber",
                                                                   "notBlank",
                                                                   "notEmpty",
                                                                   "url",
                                                                   "protocol",
                                                                   "host",
                                                                   "port",
                                                                   "regexp",
                                                                   "flags",
                                                                   "digitsize",
                                                                   "digitfraction",
                                                                   "unique",
                                                                   "precision",
                                                                   "scale");

    public String getRules(List<Variable> fieldCtx) {
        StringBuilder sb = new StringBuilder();

        // iterate over all variables in context
        for (Variable v : fieldCtx) {
            String name = v.getName();
            String value = v.getValue().toString();

            // check if we want to process this variable
            if (allowedVars.contains(name) && !"false".equals(value)) {
                sb.append(name.toLowerCase());

                // dont add unnecessary values as constraints
                if (!"true".equals(value.toLowerCase())) {

                    // append constraint value
                    sb.append("=").append(value);
                }
                // delimiter for another rule
                sb.append(";");
            }
        }
        return sb.toString();
    }

}
