
/**
 * Nahrad {[0-9]} v retezci za pozadovanou hodnotu
 * @returns {String}
 */
String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, index) {
        return typeof args[index] !== 'undefined' ? args[index] : match;
    });
};

if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}

/**
 *
 * @returns {ValidationManager}
 */
var ValidationManager = new function() {

    /**
     * Objekt, kteremu predam chybove hlasky k zobrazeni.
     *
     * @type Display
     */
    var display;

    var defaultCompositionHandler;

    this.setDisplay = function(disp) {
        if (disp === null || disp === undefined) {
            throw new Error("Null of undefined cannot be passed as parameter of "
                    + "ValidationManager#setDisplay function.");
        }
//        try {
//            disp.showAllErrors($("input").first(), new Array(), Severity[3]);
//            disp.clearAll($("input"));
        if (!disp.hasOwnProperty("showAllErrors")) {
            throw new Error("Custom display doesn't implement desired interface."
                    + " Please add the #showAllErrors public method.");
        }

        if (!disp.hasOwnProperty("clearAllErrors")) {
            throw new Error("Custom display doesn't implement desired interface."
                    + " Please add the #clearAllErrors public method.");
        }
        // TODO replace with classic for
        for (var node in constraintMap) {
            display.clearAllErrors($(escapeJsfId(node)));
        }
        display = disp;
    };

    /**
     * Map containing all required validations. Consists of node#id x array
     * of validation rules. Cacheing warning or infos is unnecessary.
     */
    var managedErrors = {};

    var constraintMap = {};

    /**
     * Mapa s validatory. Bylo by fajn ji programaticky generovat,
     * ale prozatim postaci takhle.
     */
    var validators = {
        required: new NullValidator(),
        email: new EmailValidator(),
        url: new UrlValidator(),
        date: new DateValidator(),
        past: new PastValidator(),
        future: new FutureValidator(),
        minlength: new MinLengthValidator(),
        maxlength: new MaxLengthValidator(),
        min: new MinValidator(),
        max: new MaxValidator(),
        pattern: new PatternValidator(),
        notblank: new BlankValidator(),
        creditcard: new CreditCardValidator(),
        digits: new DigitsValidator(),
        equal: new EqualValidator()
    };

    var errMsgs = {
        required: "This entry is required.",
        email: "Please enter a valid e-mail address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        past: "Please enter a past date",
        future: "Please enter a future date.",
        minlength: "Please enter at least {0} characters.",
        maxlength: "Please enter at most {0} characters.",
        min: "Please enter a value greater or equal to {0}.",
        max: "Please enter a value lower or equal to {0}.",
        pattern: "Please enter a value that matches the {0} pattern.",
        notblank: "Please enter a value that is not blank.",
        creditcard: "Please enter a valid credit card number.",
        digits: "Please enter a valid number.",
        equal: "Please enter the same value again."
    };

    // TODO remove
    this.initializeValidator = function(_constraintMap) {
        constraintMap = _constraintMap;
        var ids = Object.getOwnPropertyNames(_constraintMap);
        for (var i = 0; i < ids.length; ++i) {

            var val = $(escapeJsfId(ids[i])).val();
            // dont init for already checked fields
            if (val === undefined || val === "" || val === "0") {
                var constraints = _constraintMap[ids[i]];
                for (var j = 0; j < constraints.length; ++j) {
                    var validatorName = constraints[j].rule;
                    var validator = validators[validatorName];
                    if (validator !== undefined && validator.getPriority() === 1) {
                        if (!managedErrors[ids[i]]) {
                            managedErrors[ids[i]] = [];
                        }
                        managedErrors[ids[i]].push(validator.getRuleName());
                    }
                }
            }
        }
    };

    /**
     * Validate node value.
     *
     * @param {jQuery} node
     * @param {Array} constraints
     * @returns {Boolean}
     */
    this.checkNode = function(node, constraints) {
        //kontrola parametru
        if (node === null || node === undefined) {
            throw new Error("Cannot determine node to valide -  "
                    + "null or undefined value are not permitted.");
        }
        if (constraints === undefined) {
            throw new Error("Validation constraint must be defined.");
        }
        if (constraints === null || constraints.length === 0) {
            console.log("There are no validation constraints defined "
                    + "for node id=" + node.attr("id") + ".");
            return true;
        }
        if (defaultCompositionHandler === null || defaultCompositionHandler === undefined) {
            defaultCompositionHandler = AndConstraintHandler;
        }
        if (ValidationTemplate.checkConstraints(node, constraints, defaultCompositionHandler)) {
            return checkEmptyManagedErrors();
        }
        return false;
    };

    this.submitForm = function() {
        // TODO replace with classic for
        for (var id in managedErrors) {
            var errors = managedErrors[id].slice();
            if (errors.length > 0) {
                var node = $(escapeJsfId(id));
                for (var i = 0; i < errors.length; ++i) {
                    var constraints = constraintMap[id];
                    ValidationTemplate.checkConstraints(node, constraints, AndConstraintHandler);
                }
            }
        }

        return checkEmptyManagedErrors();
    };

    /**
     * Set default composition handler to use {AND(default), OR}.
     * @param {type} compositionType
     * @returns {undefined}
     */
    this.setCompositionHandler = function(compositionType) {
        if (!compositionType || !compositionType.type) {
            throw new Error("Composition type has to be defined.");
        }
        if (compositionType.type === "OR") {
            defaultCompositionHandler = OrConstraintHandler;
        }
        if (compositionType.type === "AND") {
            defaultCompositionHandler = AndConstraintHandler;
        }
    };

    /**
     * For testing purposes. No security harm here.
     */
    this.getCompositionHandler = function() {
        return defaultCompositionHandler;
    };

    /**
     * Quick utility method for simple validator customization.
     * @param {type} ruleName
     * @param {type} validatoinFunction
     * @param {type} priority
     * @param {type} message
     * @returns {undefined}
     */
    this.addValidationMethod = function(ruleName, validatoinFunction, priority, message) {
        this.addCustomValidator({
            getRuleName: ruleName,
            getMessage: message,
            getPriority: priority,
            validate: validatoinFunction
        });
    };

    /**
     * Add custom validator object. This validator has to define the validation, getMessage,
     * getRuleName(unique) and getPriority methods, otherwise an Error is thrown.
     * @param {type} validator
     * @returns {undefined}
     */
    this.addCustomValidator = function(validator) {
        if (validator === null || validator === undefined) {
            throw new Error("Custom validator cannot be null or undefined.");
        }
        if (!validator.hasOwnProperty("validate")) {
            throw new Error("Custom validator doesn't implement desired interface. Add #validate method.");
        }
        if (!validator.hasOwnProperty("getRuleName")) {
            throw new Error("Custom validator doesn't implement desired interface. Add #getRuleName method.");
        }
        if (!validator.hasOwnProperty("getMessage")) {
            throw new Error("Custom validator doesn't implement desired interface. Add #getMessage method.");
        }
        if (!validator.hasOwnProperty("getPriority")) {
            throw new Error("Custom validator doesn't implement desired interface. Add #getPriority method.");
        }
        var field = validators[validator.getRuleName()];
        if (field !== null && field !== undefined) {
            throw new Error("Validator with name '" + validator.getRuleName()
                    + "' is already defined. Please choose different rule name.");
        }
        validators[validator.getRuleName()] = validator;
        errMsgs[validator.getRuleName()] = validator.getMessage();
        this.initializeValidator(constraintMap);
    };

    /**
     * Sets message bundle for browsers default language. Deafault bundle for undefined languages
     * is English. Custom validator messages are not overriden.
     * @param {type} scriptPath
     * @returns {undefined}
     */
    this.setMessageLang = function(scriptPath) {
        var lang = navigator.language || navigator.userLanguage;
        lang = lang.split("-")[0];
        scriptPath += 'messages/' + lang + '-messages.js';

        // en is default language for messages
        if (lang !== 'en') {
            var callback = function(script) {
                if (messages) {
                    var props = Object.getOwnPropertyNames(messages);
                    for (var i = 0; i < props.length; ++i) {
                        errMsgs[props[i]] = messages[props[i]];
                    }
                }
                else {
                    console.info("There is no message bundle for " + lang
                            + " language. English will be used as default.");
                }
            };
            $.ajax({
                type: "GET",
                url: scriptPath,
                success: callback,
                dataType: "script",
                cache: true
            });
        }
    };

    /**
     * Predej vsechny chyby, ktere uzel obsahuje, zobrazovacimu Display objektu.
     * @param {jQuery} node
     * @param {String} messages
     * @param {Number} priority
     */
    function showAllErrorsByNode(node, messages, priority) {
        display.clearAllErrors(node);
        if (messages.length > 0) {
            display.showAllErrors(node, messages, Severity[priority]);
        }
    }

    /**
     * Remove error message from managedErrors map.
     *
     * @param {jQuery} node
     * @param {String} ruleName
     * @param {Number} severity 
     * @returns {jQuery} node
     */
    function removeError(node, ruleName, severity) {
        var nodeId = node.attr("id");
        removeErrorById(nodeId, ruleName);
    }

    function removeErrorById(nodeId, ruleName) {
        var err = managedErrors[nodeId];
        if (err !== null && err !== undefined) {
            for (var i = 0; i < err.length; ++i) {
                if (err[i] === ruleName) {
                    // cut array
                    managedErrors[nodeId].splice(i, 1);
                    --i;
                }
            }
        }
    }

    /**
     * Add error to managedErrors map.
     * @param {jQuery} node
     * @param {String} ruleName
     */
    function addError(node, ruleName) {
        var nodeId = node.attr("id");
        addErrorByNodeId(nodeId, ruleName);
    }

    function addErrorByNodeId(nodeId, ruleName) {
        var err = managedErrors[nodeId];
        if (err === null || err === undefined) {
            err = managedErrors[nodeId] = [];
        }
        if (err.length > 0) {
            for (var i = 0; i < err.length; ++i) {
                if (err[i] === ruleName) {
                    return;
                }
            }
        }
        // just push rulename into array
        managedErrors[nodeId].push(ruleName);
    }

    function checkEmptyManagedErrors() {
        // TODO replace to classic for
        for (var err in managedErrors) {
            if (managedErrors[err].length > 0) {
                return false;
            }
        }
        return true;
    }

    var AndConstraintHandler = {
        handleValidation: function(validationResult, node, ruleName, priority, msg, status) {
            var nodeId = node.attr("id");
            if (!validationResult) {
                status.addErrorMessage(msg, priority);
                // validation with priority 1 are mandatory
                if (priority === 1 || (priority !== 1 && validators.required.validate(node))) {
                    addError(node, ruleName);
                }
            }
            else {
                removeErrorById(nodeId, ruleName);
            }
            if (!validationResult && priority !== 1 && !validators.required.validate(node)) {
                removeErrorById(nodeId, ruleName);
            }
        },
        evaluateResults: function(node, status) {
            var msgs = status.currentErrorMessages;
            showAllErrorsByNode(node, msgs, status.maxPriority);
            if (msgs.length !== 0) {
                return false;
            }
            return true;
        }
    };

    var OrConstraintHandler = {
        validatioResult: false,
        handleValidation: function(validationResult, node, ruleName, priority, msg, status) {
            var nodeId = node.attr("id");
            if (validationResult) {
                status.continueValidation = false;
                this.validationResult = true;
                return;
            }
            if (!validationResult) {
                status.addErrorMessage(msg, priority);
                // validation with priority 1 are mandatory
                if (priority === 1 || (priority !== 1 && validators.required.validate(node))) {
                    addError(node, ruleName);
                }
            }
            else {
                removeErrorById(nodeId, ruleName);
            }
            if (!validationResult && priority !== 1 && !validators.required.validate(node)) {
                removeErrorById(nodeId, ruleName);
            }
        },
        evaluateResults: function(node, status) {
            if (!this.validationResult) {
                var msgs = status.currentErrorMessages;
                showAllErrorsByNode(node, msgs, status.maxPriority);
                return false;
            }
            // TODO tohle by melo jit zabalit nejak dohromady
            managedErrors[node.attr("id")] = [];
            display.clearAllErrors(node);
            this.validationResult = false; // simple reset
            return true;
        }
    };

    var ValidationTemplate = {
        checkConstraints: function(node, constraints, handler) {
            var status = {
                currentErrorMessages: [],
                maxPriority: 3,
                continueValidation: true,
                addErrorMessage: function(msg, priority) {
                    this.currentErrorMessages.push(msg);
                    if (this.maxPriority > priority) {
                        this.maxPriority = priority;
                    }
                }
            };
            for (var i = 0; i < constraints.length && status.continueValidation; ++i) {
                //parse validator name and constraints
                var validatorName = constraints[i].rule, constraint = null;
                if (constraints[i].hasOwnProperty("constraint")) {
                    constraint = constraints[i].constraint;
                }

                // get required validator
                var validator = validators[validatorName];
                if (validator === null || validator === undefined) {
                    // just log
                    console.warn("There is no matching validator for rule : " + validatorName);
                    continue;
                }

                var validationResult = validator.validate(node, constraint);
                var ruleName = validator.getRuleName();
                var msg = errMsgs[ruleName].format(constraint);
                var priority = validator.getPriority();

                handler.handleValidation(validationResult, node, ruleName, priority, msg, status);
            }
            return handler.evaluateResults(node, status);
        }
    };



    /**
     * Return jQuery ID format for jsf elements.
     * @param {String} name
     * @returns {String}
     */
    function escapeJsfId(name) {
        return "#" + name.replace(/:/g, "\\:");
    }

};



var Severity = {
    1: "error",
    2: "warn",
    3: "info"
};

var ORCompositionType = {type: "OR"};
var ANDCompositionType = {type: "AND"};