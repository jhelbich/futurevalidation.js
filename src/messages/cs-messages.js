

/**
 * Cs message bundle for future.
 * @type type
 */
var messages = {
    required: "Tento údaj je povinný.",
    email: "Prosím, zadejte platný e-mail.",
    url: "Prosím, zadejte platné URL.",
    date: "Prosím, zadejte platné datum.",
    past: "Prosím, zadejte uplynulé datum.",
    future: "Prosím, zadejte budoucí datum.",
    minlength: "Prosím, zadejte nejméně {0} znaků.",
    maxlength: "Prosím, zadejte nejvíce {0} znaků.",
    min: "Prosím, zadejte hodnotu větší nebo rovnu {0}.",
    max: "Prosím, zadejte hodnotu menší nebo rovnu {0}.",
    pattern: "Prosím, opravte tento údaj tak, aby vyhovoval vzoru {0}.",
    notblank: "Prosím zadejte neprázdou hodnotu.",
    creditcard: "Prosím, zadejte číslo kreditní karty.",
    digits: "Prosím, zadávejte pouze číslice.",
    equal: "Prosím, zadejte znovu stejnou hodnotu."
};
