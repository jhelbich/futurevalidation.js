

/**
 * En message bundle for future.
 * @type type
 */
var messages = {
    required: "This entry is required.",
    email: "Please enter a valid e-mail address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    past: "Please enter a past date",
    future: "Please enter a future date.",
    minlength: "Please enter at least {0} characters.",
    maxlength: "Please enter at most {0} characters.",
    min: "Please enter a value greater or equal to {0}.",
    max: "Please enter a value lower or equal to {0}.",
    pattern: "Please enter a value that matches the {0} pattern.",
    notblank: "Please enter a value that is not blank.",
    creditcard: "Please enter a valid credit card number.",
    digits: "Please enter a valid number.",
    equal: "Please enter the same value again."
};