
test("set custom display test", function() {
    var manager = ValidationManager;

    throws(function() {
        throw new Error("error");
    }, "throws with just a message, not using the 'expected' argument");

    throws(function() {
        manager.setDisplay();
    }, "Null of undefined cannot be passed as parameter of "
            + "ValidationManager#setDisplay function.");

    throws(function() {
        manager.setDisplay({clearAllErrors: function() {
            }});
    }, "Custom display doesn't implement desired interface."
            + " Please add the #showAllErrors public method.");

    throws(function() {
        manager.setDisplay({showAllErrors: function() {
            }});
    }, "Custom display doesn't implement desired interface."
            + " Please add the #clearAllErrors public method.");

    var testDisplay = getTestDisplay();

    manager.setDisplay(testDisplay);
    equal(testDisplay.counter, 0);

    // no errors managed so no display clear
    manager.setDisplay(testDisplay);
    equal(testDisplay.counter, 0);
});

test("add custom validator test", function() {
    var manager = ValidationManager;

    throws(function() {
        manager.addCustomValidator();
    }, "Custom validator cannot be null or undefined.");

    throws(function() {
        manager.addCustomValidator({});
    }, "Custom validator doesn't implement desired interface. Add #validate method.");

    throws(function() {
        manager.addCustomValidator({validate: function() {
            }});
    }, "Custom validator doesn't implement desired interface. Add #getRuleName method.");

    throws(function() {
        manager.addCustomValidator({
            validate: function() {
            },
            getRuleName: function() {
            }
        });
    }, "Custom validator doesn't implement desired interface. Add #getPriority method.");
    
    throws(function() {
        manager.addCustomValidator({
            validate: function() {
            },
            getRuleName: function() {
            },
            getPriority: function() {
            }
        });
    }, "Custom validator doesn't implement desired interface. Add #getMessage method.");

    var testValidator = {
        counter: 0,
        validate: function() {
        },
        getRuleName: function() {
        },
        getPriority: function() {
        },
        getMessage: function() {
        }
    };

    manager.addCustomValidator(testValidator);
    equal(testValidator.counter, 0);
});

test("or costraint composition test", function() {
    var node = getTestNode();
    var manager = ValidationManager;
    manager.setCompositionHandler(ORCompositionType);
    var testDisplay = getTestDisplay();
    manager.setDisplay(testDisplay);
    var status = getStatusObject();

    var handler = manager.getCompositionHandler();
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), false);
    equal(testDisplay.counter, 2); // clear a show
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), true);
    equal(testDisplay.counter, 1); // clear
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), true);
    equal(testDisplay.counter, 1); // clear
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), true);
    equal(testDisplay.counter, 1); // clear
});

test("and costraint composition test", function() {
    var node = getTestNode();
    var manager = ValidationManager;
    manager.setCompositionHandler(ANDCompositionType);
    var testDisplay = getTestDisplay();
    manager.setDisplay(testDisplay);
    var status = getStatusObject();

    var handler = manager.getCompositionHandler();
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), false);
    equal(testDisplay.counter, 2); // clear a show
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), false);
    equal(testDisplay.counter, 2); // clear and show
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    handler.handleValidation(false, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), false);
    equal(testDisplay.counter, 2); // clear and show
    
    testDisplay.counter = 0;
    status = getStatusObject();
    
    handler = manager.getCompositionHandler();
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    handler.handleValidation(true, node, "required", 1, "field is req", status);
    equal(handler.evaluateResults(node, status), true);
    equal(testDisplay.counter, 1); // clear
});

var testConstraintMap = {
    testInput: [
        {rule: "min", constraint: 9},
        {rule: "min", constraint: 5}
    ]
};

function getTestNode() {
    return $("#testInput");
}

function getTestDisplay() {
    var testDisplay = {
        counter: 0,
        clearAllErrors: function() {
            ++this.counter;
        },
        showAllErrors: function() {
            ++this.counter;
        }
    };
    return testDisplay;
}

function getStatusObject() {
    var status = {
        currentErrorMessages: [],
        maxPriority: 3,
        continueValidation: true,
        addErrorMessage: function(msg, priority) {
            this.currentErrorMessages.push(msg);
            if (this.maxPriority > priority) {
                this.maxPriority = priority;
            }
        }
    };
    return status;
}