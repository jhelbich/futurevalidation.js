
function getTestNode() {
    var testNode = $("#testInput");
    testNode.val("");
    return testNode;
}

test("simple parse test", function() {
    var parser = RuleParser;
    var node = getTestNode();

    node.attr("validation", "required");
    var rules = parser.loadRules(node);
    equal(Object.getOwnPropertyNames(rules).length, 1);

    node.attr("validation", "required;notnull");
    rules = parser.loadRules(node);
    equal(Object.getOwnPropertyNames(rules).length, 1);
});

test("rule count test", function() {
    var parser = RuleParser;
    var node = getTestNode();

    node.attr("validation", "required");
    var rules = parser.loadRules(node);
    for (var nodeId in rules) {
        equal(rules[nodeId].length, 1);
    }

    node.attr("validation", "required;notnull");
    rules = parser.loadRules(node);
    for (var nodeId in rules) {
        equal(rules[nodeId].length, 2);
    }

    node.attr("validation", "required;notnull;size=10;pattern(whatever)");
    rules = parser.loadRules(node);
    for (var nodeId in rules) {
        equal(rules[nodeId].length, 4);
    }
});

test("rule count test", function() {
    var parser = RuleParser;
    var node = getTestNode();
    var nodeId = node.attr("id");

    node.attr("validation", "required");
    var rules = parser.loadRules(node);
    equal(rules[nodeId][0].rule, "required");
    equal(rules[nodeId][0].constraint, undefined);
    equal(rules[nodeId][1], undefined);

    node.attr("validation", "required;notnull");
    rules = parser.loadRules(node);
    equal(rules[nodeId][0].rule, "required");
    equal(rules[nodeId][0].constraint, undefined);
    equal(rules[nodeId][1].rule, "notnull");
    equal(rules[nodeId][1].constraint, undefined);

    node.attr("validation", "required;notnull;size=10;pattern(whatever)");
    rules = parser.loadRules(node);
    equal(rules[nodeId][0].rule, "required");
    equal(rules[nodeId][0].constraint, undefined);
    equal(rules[nodeId][1].rule, "notnull");
    equal(rules[nodeId][1].constraint, undefined);
    equal(rules[nodeId][2].rule, "size");
    equal(rules[nodeId][2].constraint, 10);
    equal(rules[nodeId][3].rule, "pattern(whatever)");
    equal(rules[nodeId][3].constraint, undefined);

    node.attr("validation", "required;notnull;size=10;pattern=^[a-zA-Z =0-9*]$");
    rules = parser.loadRules(node);
    equal(rules[nodeId][0].rule, "required");
    equal(rules[nodeId][0].constraint, undefined);
    equal(rules[nodeId][1].rule, "notnull");
    equal(rules[nodeId][1].constraint, undefined);
    equal(rules[nodeId][2].rule, "size");
    equal(rules[nodeId][2].constraint, 10);
    equal(rules[nodeId][3].rule, "pattern");
    equal(rules[nodeId][3].constraint, "^[a-zA-Z =0-9*]$");
});