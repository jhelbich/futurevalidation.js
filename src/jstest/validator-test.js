var start;

QUnit.begin(function() {
	start = new Date().getTime();
});

QUnit.done(function() {
	console.log("time in millis: " + (new Date().getTime() - start));
});

function getTestNode() {
    var testNode = $("#testInput");
    testNode.val("");
    return testNode;
}

test("NullValidator test", function() {
    var validator = new NullValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node), true);

    node.val(" ");
    equal(validator.validate(node), true);

    node.val("");
    equal(validator.validate(node), false);
    
    node.val(" \n\ ");
    equal(validator.validate(node), true);
});

test("MinLengthValidator test", function() {
    var validator = new MinLengthValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node, 1), true);

    node.val(" ");
    equal(validator.validate(node, 1), true);

    node.val("");
    equal(validator.validate(node, 1), false);

    node.val("");
    equal(validator.validate(node, -1), true);
});

test("MaxLengthValidator test", function() {
    var validator = new MaxLengthValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node, 1), false);

    node.val("  ");
    equal(validator.validate(node, 1), false);

    node.val(" ");
    equal(validator.validate(node, 1), true);

    node.val("");
    equal(validator.validate(node, 1), true);

    node.val("");
    equal(validator.validate(node, -1), false);
});

test("EmailValidator test", function() {
    var validator = new EmailValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node), false);

    node.val("x @sdfc.zc");
    equal(validator.validate(node), false);

    node.val("xcd/dskl@sdfc.zc");
    equal(validator.validate(node), false);

    node.val("very.nice.email.address.yes@miail.zc");
    equal(validator.validate(node), true);

    node.val("me@god.hvn");
    equal(validator.validate(node), true);

    node.val("futurevalidatorlibraryahojjajsememailovaadresa@fel.dobryden.cvut.cz");
    equal(validator.validate(node), true);

    node.val("me@god//.cz");
    equal(validator.validate(node), false);

    node.val("erxxx-vxx.@superxxxxxx.com");
    equal(validator.validate(node), false);

    node.val("erxxx-vxx.x@superxxxxxx.com");
    equal(validator.validate(node), true);
});

test("MinValidator test", function() {
    var validator = new MinValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node, 1), true);

    node.val("1");
    equal(validator.validate(node, 1), true);

    node.val(" ");
    equal(validator.validate(node, 1), false);

    node.val("-1321534321");
    equal(validator.validate(node, 1), false);

    node.val("-0");
    equal(validator.validate(node, 1), false);

    node.val("-0");
    equal(validator.validate(node, -1), true);
});

test("MaxValidator test", function() {
    var validator = new MaxValidator();
    var node = getTestNode();

    node.val("677");
    equal(validator.validate(node, 1), false);

    node.val("-123123123");
    equal(validator.validate(node, -123123122), true);

    node.val(" ");
    equal(validator.validate(node, 1), true);

    node.val("");
    equal(validator.validate(node, 1), true);
});

test("PastValidator test", function() {
    var validator = new PastValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node, "DD.MM.YYYY"), true);

    node.val("22.2.2020");
    equal(validator.validate(node, "DD.MM.YYYY"), false);

    node.val("03-12-2020");
    equal(validator.validate(node), false);

    node.val("03-12.2004");
    equal(validator.validate(node), true);
});

test("FutureValidator test", function() {
    var validator = new FutureValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node, "DD.MM.YYYY"), false);

    node.val("22.2.2020");
    equal(validator.validate(node, "DD.MM.YYYY"), true);

    node.val("03-12-2020");
    equal(validator.validate(node), true);

    node.val("03-12.2004");
    equal(validator.validate(node), false);
});

test("PatternValidator test", function() {
    var validator = new PatternValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node, "[0-9.]*"), true);

    node.val("lucifer our lord (LOL)");
    equal(validator.validate(node, "[a-zL-O ()].*"), true);

    node.val("666 is nice");
    equal(validator.validate(node, "[a-zL-O].*"), false);

    node.val("a0");
    equal(validator.validate(node, "^[A-Za-z0-2]{1,2}$"), true);

    node.val("Zz");
    equal(validator.validate(node, "^[A-Ya-z0-2]{1,2}$"), false);

    node.val("X5");
    equal(validator.validate(node, "[A-Ya-z0-2]{1,2}"), false);
});

test("UrlValidator test", function() {
    var validator = new UrlValidator();
    var node = getTestNode();

    node.val("testurl");
    equal(validator.validate(node), false);

    node.val("testurl.c");
    equal(validator.validate(node), false);

    node.val("testurl.com");
    equal(validator.validate(node, "[a-zL-O].*"), true);

    node.val("mailto:me@god.com");
    equal(validator.validate(node, "^[A-Za-z0-2]{1,2}$"), true);

    node.val("ftp://someaddress.com/some+page_32.xhtml?someparam=23");
    equal(validator.validate(node), true);

    node.val("https://prefix.gmail.com?");
    equal(validator.validate(node), true);

    node.val("https://prefix gmail.com");
    equal(validator.validate(node), false);

    node.val("https://very%20nice.address.com");
    equal(validator.validate(node), false);
});

test("BlankValidator test", function() {
    var validator = new BlankValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node), true);

    node.val("");
    equal(validator.validate(node), false);

    node.val(" ");
    equal(validator.validate(node), false);

    node.val("          ");
    equal(validator.validate(node), false);

    node.val("        \n\            ");
    equal(validator.validate(node), false);
});

test("CreditCardValidator test", function() {
    var validator = new CreditCardValidator();
    var node = getTestNode();

    node.val("312681121132156"); // just some number
    equal(validator.validate(node), false);
    node.val("666666666666666666"); // just some number
    equal(validator.validate(node), false);
    node.val("asdfffdsasdf"); // just something
    equal(validator.validate(node), false);

    node.val("378282246310005"); // American Express
    equal(validator.validate(node), true);
    node.val("371449635398431");
    equal(validator.validate(node), true);
    node.val("378734493671000");
    equal(validator.validate(node), true);

    node.val("5610591081018250"); // Australian BankCard
    equal(validator.validate(node), true);

    node.val("30569309025904"); // Diners Club
    equal(validator.validate(node), true);
    node.val("38520000023237");
    equal(validator.validate(node), true);

    node.val("6011111111111117"); // Discover
    equal(validator.validate(node), true);
    node.val("6011000990139424");
    equal(validator.validate(node), true);

    node.val("3530111333300000"); // JCB
    equal(validator.validate(node), true);
    node.val("3566002020360505");
    equal(validator.validate(node), true);

    node.val("5555555555554444"); // MasterCard
    equal(validator.validate(node), true);
    node.val("5105105105105100");
    equal(validator.validate(node), true);

    node.val("4111111111111111"); // Visa
    equal(validator.validate(node), true);
    node.val("4012888888881881");
    equal(validator.validate(node), true);
    node.val("4222222222222");
    equal(validator.validate(node), true);
});

test("DigitsValidator test", function() {
    var validator = new DigitsValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node), false);

    node.val("22.2");
    equal(validator.validate(node), true);

    node.val("22");
    equal(validator.validate(node), true);

    node.val("22,2");
    equal(validator.validate(node), true);

    node.val("22,2a");
    equal(validator.validate(node), false);

    node.val("a22.2");
    equal(validator.validate(node), false);

    node.val("one");
    equal(validator.validate(node), false);

    node.val("22,2e2");
    equal(validator.validate(node), true);

    node.val("22,2x2");
    equal(validator.validate(node), false);
});

test("EqualValidator test", function() {
    var validator = new EqualValidator();
    var node = getTestNode();
    node.after('<input type="hidden" id="someInput" />');
    var equalNode = $("#someInput");
    var equalId = equalNode.attr("id");

    node.val("22.2.2002");
    equalNode.val("666");
    equal(validator.validate(node, equalId), false);

    node.val("22.2.2002");
    equalNode.val("22.2.2002");
    equal(validator.validate(node, equalId), true);

    node.val("2002");
    equalNode.val(2002);
    equal(validator.validate(node, equalId), true);

    node.val("hi");
    equalNode.val("Hi");
    equal(validator.validate(node, equalId), false);

    node.val("hello and good  night");
    equalNode.val("hello and good night");
    equal(validator.validate(node, equalId), false);
});

test("DateValidator test", function() {
    var validator = new DateValidator();
    var node = getTestNode();

    node.val("22.2.2002");
    equal(validator.validate(node, "DD.MM.YYYY"), true);

    node.val("22.2/2020");
    equal(validator.validate(node, "DD.MM/YYYY"), true);

    node.val("03-12-2020");
    equal(validator.validate(node), true);

    node.val("03-12.2004");
    equal(validator.validate(node), true);

    node.val("13.3-200");
    equal(validator.validate(node, "MM.DD-YYYY"), false);

    node.val("13.3-200");
    equal(validator.validate(node, "DD.MM-YYYY"), true);

    node.val("29.2.2011");
    equal(validator.validate(node, "DD.MM.YYYY"), false);

    node.val("29.2.2004");
    equal(validator.validate(node, "DD.MM.YYYY"), true);

    node.val("May 22nd 2014, 2:47:14 am");
    equal(validator.validate(node, "MMMM Do YYYY, h:mm:ss a"), true);

    node.val("2013-039T09:30:26.123");
    equal(validator.validate(node), true);

    node.val("2013-W06-5T09");
    equal(validator.validate(node), true);

    node.val("30/3/2666");
    equal(validator.validate(node, "DD-MM-YYYY"), true);
});
