
var display = new Display();

test("show all errors test", function() {
    $("body").append('<input type="hidden" id="testInput" />');
    var node = $("#testInput");
    var testMsg = "test msg";
    var severity = "warning";

    display.showAllErrors(node, [testMsg], severity);

    var errorNode = $("div.errorMessagetestInput");
    equal(errorNode.length, 1);

    var childrenSpans = errorNode.children("span");
    equal(childrenSpans.length, 1);
    equal(childrenSpans.html(), testMsg + " ");
    node.remove();
    errorNode.remove();
    childrenSpans.remove();
});

test("clear all errors test", function() {
    $("body").append('<input type="hidden" id="testInput" />');
    var node = $("#testInput");

    display.clearAllErrors(node);

    var errorNode = $("div.errorMessagetestInput");
    equal(errorNode.length, 1);

    var childrenSpans = errorNode.children("span");
    equal(childrenSpans.length, 0);

    equal(errorNode.html(), "");
    
    node.remove();
    errorNode.remove();
    childrenSpans.remove();
});

test("clear and show test", function() {
    $("body").append('<input type="hidden" id="testInput" />');
    var node = $("#testInput");
    var testMsg = "test msg";
    var severity = "warning";
    
    display.clearAllErrors(node);

    var errorNode = $("div.errorMessagetestInput");
    equal(errorNode.length, 1);

    var childrenSpans = errorNode.children("span");
    equal(childrenSpans.length, 0);

    equal(errorNode.html(), "");
    
    display.showAllErrors(node, [testMsg], severity);
    
    errorNode = $("div.errorMessagetestInput");
    equal(errorNode.length, 1);

    childrenSpans = errorNode.children("span");
    equal(childrenSpans.length, 1);
    equal(childrenSpans.html(), testMsg + " ");
    
    node.remove();
    errorNode.remove();
    childrenSpans.remove();
});
