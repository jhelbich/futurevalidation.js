
/**
 * Zobrazeni chybove hlasky v zavislosti na implementaci JSF.
 * Input musi byt uzavreny v
 * <div><input /> <div id="errorMessage" /></div> bloku.
 *
 * @returns {Display}
 */
var Display = function() {
    
    /**
     * jQuery id selector.
     * @type String
     */
    var errorElementId = '[class^="errorMessage"]';

    /**
     *
     * @param {jQuery} node
     * @returns {jQuery}
     */
    function findErrorNode(node) {
        var errorNode = node.siblings(errorElementId);
        if (checkNullNode(errorNode)) {
            errorNode = node.parent().siblings(errorElementId);
            if (checkNullNode(errorNode)) {
                node.after('<div class="errorMessage' + node.attr("id") + '"></div>');
                errorNode = node.next(errorElementId);
            }
        }
        return errorNode;
    }

    function checkNullNode(node) {
        return node === null || node === undefined || node.length === 0;
    }

    /**
     * Clear validation messages.
     * @param {jQuery} node to validate
     */
    this.clearAllErrors = function(node) {
        if (!node.attr("id")) {
            return;
        }
        var errorNode = findErrorNode(node);
        errorNode.html("");
    };

    /**
     * Display content of errorMessages parameter.
     * @param {jQuery} node
     * @param {Array} errorMessages
     * @param {String} severity error message severity level
     */
    this.showAllErrors = function(node, errorMessages, severity) {
        for (var i = 0; i < errorMessages.length; ++i) {
            var message = errorMessages[i];
            findErrorNode(node).append("<span style='color : red'>" + message + " </span>");
        }
    };

};
