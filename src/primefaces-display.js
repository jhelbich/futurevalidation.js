

/*
 * <div id="personForm1:emailErrMsg" class="ui-message-error ui-widget ui-corner-all">
 *      <span class="ui-message-error-icon"></span>
 *      <span class="ui-message-error-detail">may not be empty</span>
 * </div>
 */


/**
 * Validation messages display for PrimeFaces framework, version 3.4.2.
 * Servers as default primefaces error message display
 * @returns {PrimeFacesDisplay}
 */
var PrimeFacesDisplay = function() {

    var growlDisplay = new GrowlDisplay();
    var nodeMsgCache = {};
    var errMsgBase = "jqv-errmsg-";
    var errorMessageClasses = ["ui-message-", "ui-widget", "ui-corner-all"];
    var errorMessageSpans = '<span class="ui-message-error-icon"></span><span class="ui-message-error-detail"></span>';

    /**
     * Clear validation messages for node.
     * @param {jQuery} node to validate
     */
    this.clearAllErrors = function(node) {
        if (node === null || node === undefined) {
            return;
        }
        var errNode = nodeMsgCache[escapeJsfId(node.attr("id"))];
        if (errNode !== null && errNode !== undefined) {
            errNode.hide();
        }
        if (node.hasClass("ui-state-error")) {
            node.removeClass("ui-state-error");
        }
    };

    /**
     * Display content of errorMessages parameter.
     * @param {jQuery} node
     * @param {String} severity message severity level - either "error", "warn" of "info"
     * @param {Array} errorMessages
     */
    this.showAllErrors = function(node, errorMessages, severity) {
        if (severity === null || severity === undefined) {
            severity = getMessageSeverity(node);
        }
        for (var i = 0; i < errorMessages.length; ++i) {
            var message = errorMessages[i];
            displayErrorNode(node, message, severity);
//            growlDisplay.displayGrowl(message, severity, node);
        }
        // add error-state class to node
        node.addClass("ui-state-error");
    };

    /**
     * Display error message as <p:messages> jsf tag would render it.
     * @param {jQuery} node
     * @param {String} message
     * @param {String} severity
     */
    function displayErrorNode(node, message, severity) {
        var errNode = findErrorNode(node, message, severity);
        var nodeClass = errNode.attr("class");
        if (nodeClass.indexOf(severity) < 0) {
            errNode.remove();
            nodeMsgCache[escapeJsfId(node.attr("id"))] = null;
            errNode = findErrorNode(node, message, severity);
        }
        errNode.children(".ui-message-" + severity + "-detail").text(message);
        adjustErrorMessageWidth(errNode, message, severity);
        errNode.show();
    }

    function escapeJsfId(name) {
        return "#" + name.replace(/:/g, "\\:");
    }

    /**
     *
     * @param {jQuery} node
     * @param {String} message
     * @param {String} severity severity level
     * @returns {jQuery}
     */
    function findErrorNode(node, message, severity) {
        var nodeId = escapeJsfId(node.attr("id"));
        // check, if the error node is in cache
        var errNode = nodeMsgCache[nodeId];
        if (isNullUndefinedOrEmpty(errNode)) {
            errNode = node.siblings(nodeId);
            if (isNullUndefinedOrEmpty(errNode)) {
                errNode = node.parent().siblings(nodeId);
                if (isNullUndefinedOrEmpty(errNode)) {
                    errNode = getErrorNode(node, severity);
                    node.after(errNode);
                    nodeMsgCache[nodeId] = errNode;
                }
            }
        }
        return errNode;
        /*
         if (!isNullUndefinedOrEmpty(errNode)) {
         var errId = errNode.attr("id");
         if ($(escapeJsfId(errId)).length > 0) {
         return errNode;
         }
         
         }
         
         // is our node a part of a table?
         var isInTable = false;
         var parent = node.parents("td").first();
         if (parent.length > 0 && !isNullUndefinedOrEmpty(parent)) {
         isInTable = true;
         }
         
         // if our node is in table, append error message to the end
         // of this table a cache it
         // otherwise try to find (or create) error node and append it to our node
         errNode = findFirstEmptyDiv(isInTable ? parent : node);
         
         if (isNullUndefinedOrEmpty(errNode)) {
         errNode = createNewErrorNode(node, isInTable, severity);
         }
         else {
         checkErrorNodeClasses(errNode);
         checkErrorNodeContent(errNode);
         }
         // if our error node is outside of table, it creates a disgusting mess
         // so this beautiful 'if' measures displayed text and sets nodes width
         // still doesn't work properly, though
         adjustErrorMessageWidth(errNode, message, severity);
         nodeMsgCache[nodeId] = errNode;
         return errNode;
         */
    }

    /**
     * Check for null, undefined or empty.
     * @param {Object} object
     * @returns {Boolean} true, if node === null || node === undefined
     */
    function isNullUndefinedOrEmpty(object) {
        return object === null || object === undefined || object === "" || object.length === 0;
    }

    /**
     * Create new error node for displaying validation messages.
     * @param {jQuery} node
     * @param {Boolean} isInTable
     * @param {String} severity severity level
     * @returns {jQuery} new error node for message display
     */
    function createNewErrorNode(node, isInTable, severity) {
        var errNode = getErrorNode(node, severity);

        if (isInTable) {
            // create new column at the right side of the table
            var lastCol = node.parents("tr");
            lastCol.append("<td></td>"); // tohle vybirani je hrozne
            lastCol.children("td").last().append(errNode);
        }
        else {
            node.after(errNode);
        }
        return errNode;
    }

    /**
     * Search for error node down the element tree.
     * @param {jQuery} startSearchNode
     * @returns {jQuery}
     */
    function findFirstEmptyDiv(startSearchNode) {
        var siblings = startSearchNode.siblings().andSelf();
        for (var i = 0; i < siblings.length; ++i) {
            var currentNode = $(siblings[i]);
            if (currentNode.is("div")
                    && (isNullUndefinedOrEmpty(currentNode.val()))
                    && currentNode.children().length === 0) {
                return currentNode;
            }
            if (currentNode.children().length > 0) {
                var fromChildren = findFirstEmptyDiv(currentNode.children().first());
                if (!isNullUndefinedOrEmpty(fromChildren)) {
                    return fromChildren;
                }
            }
        }
        return null;
    }

    /**
     * Check if errNode contains correct class attributes.
     * @param {jQuery} errNode
     * @returns {void}
     */
    function checkErrorNodeClasses(errNode) {
        var classes = errNode.attr("class");
        if (isNullUndefinedOrEmpty(classes)) {
            classes = ""; // hehe
        }
        for (var i = 0; i < errorMessageClasses.length; ++i) {
            if (classes.indexOf(errorMessageClasses[i]) < 0) {
                errNode.addClass(errorMessageClasses[i]);
            }
        }
    }

    /**
     * Check if errNode is the parent for error message display element.
     * @param {jQuery} errNode
     * @returns {void}
     */
    function checkErrorNodeContent(errNode) {
        var spanChildren = errNode.children("span.ui-message-error-detail");
        if (isNullUndefinedOrEmpty(spanChildren) || spanChildren.length <= 0) {
            errNode.append(errorMessageSpans);
        }
    }

    /**
     * Return jQuery-ui error element for defined severity level.
     * @param {jQuery} node
     * @param {String} severity severity level
     * @returns {jQuery}
     */
    function getErrorNode(node, severity) {
        if (severity === undefined) {
            severity = getMessageSeverity(node);
        }
        var errNd = '<div id="' + errMsgBase + node.attr("id") + '" class="ui-message-' + severity + ' ui-widget ui-corner-all">';
        errNd += '<span class="ui-message-' + severity + '-icon"></span>';
        errNd += '<span class="ui-message-' + severity + '-detail"></span>';
        errNd += '</div>';
        // hack, vrati samostatny errNd, tag <div/> neexistuje
        return $('<div/>').html(errNd).contents();
    }


    /**
     * Get text width based on font.
     * @param {String} text
     * @param {String} font
     * @returns {Integer}
     */
    function getTextWidth(text, font) {
        var canvas = $("canvas")[0];
        if (canvas === null || canvas === undefined) {
            canvas = document.createElement("canvas");
        }
        var ctx = canvas.getContext("2d");
        ctx.font = font;
        var metrics = ctx.measureText(text);
        return metrics.width;
    }

    /**
     * Adjust element width to displayed message.
     * @param {jQuery} errNode
     * @param {String} message
     * @param {String} severity
     */
    function adjustErrorMessageWidth(errNode, message, severity) {
        if (severity === undefined) {
            // if not defined, suppose error
            severity = "error";
        }
        var family = errNode.css("font-family");
        var size = errNode.css("font-size");
        var style = errNode.css("font-style");
        var font = style + " " + size + " " + family;
        var textWidth = getTextWidth(message, font);
        var iconSpan = errNode.children(".ui-message-" + severity + "-icon").first();
        if (iconSpan.length > 0) {
            textWidth += parseInt((iconSpan.css("width").split("px")));
            textWidth += parseInt(iconSpan.css("margin-left").split("px"));
            textWidth += parseInt(iconSpan.css("margin-right").split("px"));
        }
        var leftMargin = parseInt(errNode.css("margin-left").split("px"));
        var rightMargin = parseInt(errNode.css("margin-right").split("px"));
        textWidth += leftMargin + rightMargin;
        errNode.css("width", textWidth);
    }

    /**
     * Covers basic validations, that are requiered.
     * @param {jQuery} node
     * @returns {String} severity level
     */
    function getMessageSeverity(node) {
        var req = node.attr("required");
        var validations = node.attr("validation");
        // should not happen, but, well, whatever
        if (validations === null || validations === undefined) {
            return req ? "error" : "warn";
        }
        var isNotNull = validations.indexOf("notnull") >= 0;
        var isRequired = validations.indexOf("required") >= 0;
        var isNotBlank = validations.indexOf("notblank") >= 0;
        var severity = req || isNotNull || isRequired || isNotBlank ? "error" : "warn";
        return severity;
    }

};

var GrowlDisplay = function() {

    /**
     * Display error message in growl.
     * @param {String} message
     * @param {String} severity
     */
    this.displayGrowl = function(message, severity, node) {
        var growlId = "jqv-errmsg-growl";
        var widgetName = "widget_" + growlId;
        var growl = window[widgetName];
        if (growl === null || growl === undefined || growl.length === 0) {
            growl = createNewGrowl(node, widgetName, growlId);
            if (!$.easing['easeInOutCirc']) {
                loadJQueryPlugins(renderGrowlMessage(growl, message, severity));
                return;
            }
        }
        renderGrowlMessage(growl, message, severity);
    };

    function renderGrowlMessage(growl, message, severity) {
        growl.renderMessage(
                {
                    summary: "",
                    detail: message,
                    severity: severity
                }
        );
    }

    function createNewGrowl(node, widgetName, growlId) {
        node.parents("form").append('<span id="' + growlId + '></span>');
        PrimeFaces.cw('Growl', widgetName,
                {
                    id: growlId,
                    sticky: false,
                    life: 6000, // default primefaces
                    escape: true,
                    msgs: []
                }
        );
        return window[widgetName];
    }

    function loadJQueryPlugins(callback) {
        var appMeta = $("meta[name=aplicationPath]");
        if (appMeta !== null && appMeta !== undefined) {
            var appPath = appMeta.attr("content");
            appPath += "/javax.faces.resource/jquery/jquery-plugins.js.xhtml?ln=primefaces";
            try {
                getScript(appPath, callback);
            } catch (ex) {
                console.error("Blah blah blah, no script at " + appPath + ", madafakaaaa~~~~");
            }
        }
    }

    function getScript(url, callback) {
        $.ajax({
            type: "GET",
            url: url,
            success: callback,
            dataType: "script",
            cache: true
        });
    }
};