// get script location for AJAX message budles loading
// executed at include time
var scripts = document.getElementsByTagName('script');
// remove any ?query
var path = scripts[scripts.length - 1].src.split('?')[0];
// remove last filename part of path
var msgsDir = path.split('/').slice(0, -1).join('/') + '/';

/**
 * Main object handles form submits, default display and resources loading.
 * @param {type} elemsToValidate
 * @returns {Main}
 */
var Main = function(elemsToValidate) {

    var validatedElems = elemsToValidate;

    var inputsBinded = false;

    // try to get implicit jsf display for current implementation
    var jsfImpl = $("meta[name=jsf-implementation]").attr("content");
    if (jsfImpl === null || jsfImpl === undefined || jsfImpl === "") {
        console.log("JSF component framework should be specified in <meta>.");
        useRawImpl();
    }
    else if (jsfImpl.indexOf("primefaces") >= 0) {
        if (typeof PrimeFacesDisplay === "undefined") {
            var split = jsfImpl.split("-");
            var scriptUrl = "resources/js/" + split[0] + "-display-" + split[1] + ".js";
            try {
                getScript(scriptUrl, usePrimeFacesImpl());
            } catch (e) {
                useDefaultPrimefacesImpl();
            }
        }
        else {
            usePrimeFacesImpl();
        }
    }
    // default display, if jsfImpl Display Script doesn't exist
    else {
        useRawImpl();
    }


    this.initializeInputs = function() {
        if (!$.isEmptyObject(validatedElems)) {
            ValidationManager.initializeValidator(validatedElems);
            ValidationManager.setMessageLang(msgsDir);
            // disable all buttons in forms
            disableButtonState();
            if (!inputsBinded) {
                $(document).on("blur", "input[validation]", function() {
                    validateInputs(this, validatedElems);
                });
                inputsBinded = true;
            }
        }
    };

    function validateInputs(field, elemsToValidate) {
        var valArr = Object.getOwnPropertyDescriptor(elemsToValidate, field.id);
        // may happen if there are no validation elements present on page
        if (valArr !== null && valArr !== undefined) {
            valArr = valArr.value;
            if (definedNotEmpty(valArr)) {
                ValidationManager.checkNode($(field), valArr);
            }
        }
    }

    function definedNotEmpty(elem) {
        return elem !== null && elem !== undefined && elem.length > 0;
    }

    function disableButtonState() {
        var formParents = $("input[validation]").parents("form");
        var submits = formParents.find("input[type=submit]");
        submits.on("click", validateFormSubmit);
        var buttons = formParents.find("button[type=submit]");
        buttons.on("click", validateFormSubmit);
    }

    function validateFormSubmit() {
        var isFormValid = ValidationManager.submitForm();
        return isFormValid;
    }

    // display choice - scheduled to deprecate in next release
    function usePrimeFacesImpl() {
        if (typeof PrimeFacesDisplay === "undefined") {
            throw new Error("PrimeFaces display has not been initialized yet.");
        }
        ValidationManager.setDisplay(new PrimeFacesDisplay());
        this.initializeInputs;
    }

    function useRawImpl() {
        ValidationManager.setDisplay(new Display());
        this.initializeInputs;
    }

    function useDefaultPrimefacesImpl() {
        var scriptUrl = "resources/js/primefaces-display.js";
        try {
            getScript(scriptUrl, usePrimeFacesImpl());
        } catch (e) {
            throw new Error("Unable to find validation message display for jsf impl : " + jsfImpl);
        }
    }

    function getScript(url, callback) {
        $.ajax({
            type: "GET",
            url: url,
            success: callback,
            dataType: "script",
            cache: true
        });
    }


};