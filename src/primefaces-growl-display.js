
var PrimeFacesGrowlDisplay = function() {
    
    this.clearAllErrors = function(node) {
    };

    /**
     * Display content of errorMessages parameter.
     * @param {jQuery} node
     * @param {Array} errorMessages
     */
    this.showAllErrors = function(node, errorMessages, severity) {
        for (var i = 0; i < errorMessages.length; ++i) {
            var message = errorMessages[i];
            displayGrowl(message, severity, node);
        }
    };

    /**
     * Display error message in growl.
     * @param {String} message
     * @param {String} severity
     * @param {jQuery} node input node
     */
    function displayGrowl(message, severity, node) {
        var growlId = "jqv-errmsg-growl";
        var widgetName = "widget_" + growlId;
        var growl = window[widgetName];
        if (growl === null || growl === undefined || growl.length === 0) {
            growl = createNewGrowl(node, widgetName, growlId);
            if (!$.easing['easeInOutCirc']) {
                loadJQueryPlugins(renderGrowlMessage(growl, message, severity));
                return;
            }
        }
        renderGrowlMessage(growl, message, severity);
    }

    function renderGrowlMessage(growl, message, severity) {
        growl.renderMessage(
                {
                    summary: "",
                    detail: message,
                    severity: severity
                }
        );
    }

    function createNewGrowl(node, widgetName, growlId) {
        node.parents("form").append('<span id="' + growlId + '></span>');
        PrimeFaces.cw('Growl', widgetName,
                {
                    id: growlId,
                    sticky: false,
                    life: 6000, // default primefaces
                    escape: true,
                    msgs: []
                }
        );
        return window[widgetName];
    }

    function loadJQueryPlugins(callback) {
        var appMeta = $("meta[name=aplicationPath]");
        if (appMeta !== null && appMeta !== undefined) {
            var appPath = appMeta.attr("content");
            appPath += "/javax.faces.resource/jquery/jquery-plugins.js.xhtml?ln=primefaces";
            try {
                getScript(appPath, callback);
            } catch (ex) {
                console.error("There is no script located at " + appPath + ", please check defined path.");
            }
        }
    }

    function getScript(url, callback) {
        $.ajax({
            type: "GET",
            url: url,
            success: callback,
            dataType: "script",
            cache: true
        });
    }
};