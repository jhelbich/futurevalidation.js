
var ValidationClass = {
    required: 1,
    optional: 2
};

var NullValidator = function() {

    var ruleName = "required";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        var val = node.val();
        return !($.isEmptyObject(val) || val === "");
    };
};

var MinLengthValidator = function() {
    
    var ruleName = "minlength";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    /**
     *
     * @param {jQuery} node
     * @param {Number} minLength
     * @returns {Boolean}
     */
    this.validate = function(node, minLength) {
        if (minLength === null || minLength === undefined) {
            throw new Error("MinLengthValidator cannot be called with null parameters.");
        }
        return node.val().length >= minLength;
    };
};

var MaxLengthValidator = function() {

    var ruleName = "maxlength";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, maxLength) {
        if (maxLength === null || maxLength === undefined) {
            throw new Error("MaxLengthValidator cannot be called with null parameters.");
        }
        return node.val().length <= maxLength;
    };
};

var DateValidator = function() {

    var defaultFormat = "MM-DD-YYYY";
    var priority = ValidationClass.optional;
    var ruleName = "date";

    this.getRuleName = function() {
        return ruleName;
    };

    this.getPriority = function() {
        return priority;
    };

    this.setDefaultPattern = function(pattern) {
        defaultFormat = pattern;
    };

    this.validate = function(node, format) {
        if (format !== null && format !== undefined && format !== "") {
            return this.getDate(node, format).isValid();
        }
        return this.getDate(node).isValid();
    };

    this.getDate = function(node, format) {
        if (format !== null && format !== undefined && format !== "") {
            return moment(node.val(), format);
        }
        var value = node.val();
        var m = moment(value, defaultFormat);
        return m.isValid() ? m : moment(value);
    };

};

var EmailValidator = function() {

    //TODO zkontrolovat regexp
    var pattern = new RegExp("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    var ruleName = "email";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        return pattern.test(node.val());
    };
};

var MaxValidator = function() {

    var ruleName = "max";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        if (constraint === null) {
            throw new Error("MaxValidator cannot be called with null parameters.");
        }
        var numConstraint = Number(constraint);
        if (isNaN(numConstraint)) {
            throw new Error("Error in validation - @Min argument must be numeric.");
            return true;
        }
        var numValue = Number(node.val().replace(",", "."));
        return numValue <= numConstraint;
    };
};

var MinValidator = function() {

    var ruleName = "min";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        if (constraint === null) {
            throw new Error("MinValidator cannot be called with null parameters.");
        }
        var numConstraint = Number(constraint);
        if (isNaN(numConstraint)) {
            throw new Error("Error in validation - @Min argument must be numeric.");
            return true;
        }
        var numValue = Number(node.val().replace(",", "."));
        return numValue >= numConstraint;
    };
};

var PastValidator = function() {

    var dateValidator = new DateValidator();

    var ruleName = "past";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, format) {
        if (!dateValidator.validate(node, format)) {
            return false;
        }
        var date = dateValidator.getDate(node, format);
        return date.isBefore(moment());
    };

};

var FutureValidator = function() {

    var dateValidator = new DateValidator();

    var ruleName = "future";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, format) {
        if (!dateValidator.validate(node, format)) {
            return false;
        }
        var date = dateValidator.getDate(node, format);
        return date.isAfter(moment());
    };
};

var PatternValidator = function() {
    
    var ruleName = "pattern";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };
    
    /**
     *
     * @param {jQuery} node
     * @param {String} pattern
     * @returns {Boolean}
     */
    this.validate = function(node, pattern) {
        if(!pattern) { 
            throw new Error("Regular expression patter has to be specified.");
        }
        pattern = "^" + pattern + "$"; // primitivni, ale ucinne
        var regexp = new RegExp(pattern, "g");
        var result = regexp.test(node.val());
        return result;
    };
};

// @hibernate.validator.constraints
var UrlValidator = function() {

    var fullUrlExp = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;

    var protocolExp = /^[^:]+(?=:\/\/)/;

    var ruleName = "url";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node) {
        var value = node.val();
        var result = fullUrlExp.test(value);
        if (result && value.indexOf("mailto:") <= 0) {
            // kontrola, jestli url obsahuje protokol
            // primefaces validace ho jinak nepusti dal
            if (!protocolExp.test(value)) {
                node.val("http://" + value);
            }
        }
        return result;
    };
};

var BlankValidator = function() {

    var ruleName = "notblank";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.required;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        var value = node.val();
        value = value.replace(/\s/g, "");
        if ($.isEmptyObject(value) || value === "") {
            return false;
        }
        return true;
    };
};

var CreditCardValidator = function() {

    var ruleName = "creditcard";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    var blankValidator = new BlankValidator();

    this.validate = function(node, constraint) {
        var value = node.val();
        if (!blankValidator.validate(node)) {
            return false;
        }
        var result = luhnAlgorithm(value);
        return result;
    };

    // taken from https://gist.github.com/DiegoSalazar/4075533 , much thx

    function luhnAlgorithm(value) {
        // accept only digits, dashes or spaces
        if (/[^0-9-\s]+/.test(value))
            return false;

        // The Luhn Algorithm. It's so pretty.
        var nCheck = 0, nDigit = 0, bEven = false;
        value = value.replace(/\D/g, "");

        for (var n = value.length - 1; n >= 0; n--) {
            var cDigit = value.charAt(n),
                    nDigit = parseInt(cDigit, 10);

            if (bEven) {
                if ((nDigit *= 2) > 9)
                    nDigit -= 9;
            }

            nCheck += nDigit;
            bEven = !bEven;
        }

        return (nCheck % 10) === 0;
    }
};

var DigitsValidator = function() {

    var ruleName = "digits";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        var value = node.val();
        // for specific decimal formats
        value = value.replace(",", "."); 
        var result = !isNaN(parseFloat(value)) && isFinite(value);
        return result;
    };
};

var EqualValidator = function() {

    var ruleName = "equal";

    this.getRuleName = function() {
        return ruleName;
    };

    var priority = ValidationClass.optional;

    this.getPriority = function() {
        return priority;
    };

    this.validate = function(node, constraint) {
        if(!constraint) {
            throw new Error("Node id for equal validation has to be specified.");
        }
        var otherNode = $("#" + constraint);
        if(!otherNode || otherNode.length === 0) {
            console.error("There is no such node with id: " + constraint);
            return false;
        }
        var value = node.val();
        var otherNodeValue = otherNode.val();
        return value === otherNodeValue;
    };
};
