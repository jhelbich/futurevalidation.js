
$(function() {
    var elemsToValidate = RuleParser.loadRules($("input[validation]"));
    var main = new Main(elemsToValidate);
    main.initializeInputs();
});
