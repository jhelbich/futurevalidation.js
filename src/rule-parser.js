/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var RuleParser = {
    /**
     *
     * @param {jQuery} nodes to validate {array}
     * @returns {Object} rulePairMap: node ID (key) - rules array (value)
     */
    loadRules: function(nodes) {
        var rules = {};
        if (nodes === null || nodes === undefined) {
            // nebude lepsi vyhodit chybu?
            console.log("There are no nodes with 'validation' attribute.");
            return rules;
        }
        for (var i = 0; i < nodes.length; ++i) {
            var node = nodes[i];
            var nodeId = $(node).attr("id");
            //inicializace pole
            rules[nodeId] = [];
            // TODO parsovani napr. pro regexp - muze obsahovat strednik
            var content = $(node).attr("validation").split(";");
            if (content !== null && content.length > 0) {
                for (var j = 0; j < content.length; ++j) {
                    var trimmed = content[j].trim();
                    if (trimmed !== null && trimmed !== "") {
                        var split = trimmed.split(this.splitChar);
                        var validationRule = {rule: split[0]};
                        if (split[1] !== undefined && split[1] !== null && split [1] !== "") {
                            validationRule.constraint = this.appendConstraint(split);
                        }
                        rules[nodeId].push(validationRule);
                    }
                }
            }
        }
        return rules;
    },
    
    appendConstraint: function(split) {
        for (var i = 2; i < split.length; ++i) {
            split[1] += this.splitChar + split[i];
        }
        return split[1];
    },
    
    splitChar : "="

};
