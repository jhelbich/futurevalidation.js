##Overview
*__futurevalidation.js__* is a generic javascript library for client side form data validation. It already
supports large scale of validations but is still easily pluggable with everything
you need for validating data.

##How it works
Library registers all `<input>` HTML tags that have attribute 'validation' defined. When this
registered input loses focus, its value is validated base on rules defined in 'validation'
attribute. Supported rules are: 

       * notnull
       * email
       * date
       * past
       * future
       * minlength
       * maxlength
       * min
       * max
       * pattern
       * url
       * notblank
       * creditcard
       * digits
       * equal

##Getting it to work

As of now, scripts are minified in the /lib directory, so you need to add the future.min.js
to the HTML `<head`> element.
Scripts: 

```
    <script type="text/javascript" src="lib/future.min.js" ></script>
    
```

If you would like to use predefined display scripts, which are targeted at jsf libraries such as
Primefaces, add another meta tag in your `<head>` element: 

```
    <meta name="jsf-implementation" content="#{metaUtils.library}-#{metaUtils.version}" />
```
where #{metaUtils.library} is the library's name (e.g. "primefaces") and #{metaUtils.version}
contains its version, which may matter in some implementations. If there is none prepared
diplay implementation, plain one is given as default and error is logged. I advise you to
check error logs, you may find valuable information and save a lot of time while getting
*future* to work.

Last step is to define 'validation' attibute for all `<input>` fields, which values
are to be checked. Syntax for rules is '<rulename>=<constraint>;'. Rules are delimited 
by the [;] character. For some static rules, adding constraint is not necessary, so
just '<rulename>;' is enough.

```
    <input id="my-input" type="text" validation="required;email" />
```
or
```
    <input id="my-input" type="text" validation="required;past=dd.MM.yyyy;" />
```

After page reload, you should already see the magic happen.

##Pluggable objects
###Custom validators
If you would like to register your own validator, all you have to do is keep it compatible
with interface &future uses and register it. Managing validators is responsibility of
ErrorManager object. Try calling ErrorManager.addCustomValidator with your validator object
as parameter. By default, every custom validator has its interface checked so there are
no errors in the validation stage based on object mismatch. The required interface consists
of 4 public methods: 

```
    // returns error message if validation fails
    validator.getMessage() 

    // priority is either 1 (required) or 2(optional), use ValidationClass object  for definition
    validator.getPriority() 

    // return unique rule name, which reffers to rules in 'validation' attribute of `<input>` element
    validator.getRuleName() 

    // returns result of validation as boolean, parameter is <input> element wrapped by jQuery
    validator.validate(jQuery)
```

###Dislaying error message
Covering error displaying for every kind of page is an impossible task. It is expected that you
will want to create your own display object. Its pretty simple, all requierments are two 
methods: 

```
    display.showAllErrors(element, [messages], severity)

    display.clearAllErrors(element)
```

Usage of these methods is selfexplanory. Register your display object through 
ErrorManager.setDisplay(display) function.

There are few display implementations prepered, but I don't expect you to really use them
as they are. I advise you, though, to have a look at them just to see how displays are working.

It is not expected, though, that you will use predefided displays or that prepared 
validators will be enough for your application. Read about customizing library's usage
in next section.